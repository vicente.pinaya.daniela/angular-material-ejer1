import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InterfaceI } from 'src/app/interfaz/interface.datos';

@Injectable({
  providedIn: 'root'
})
export class TreeService {

  constructor(public http: HttpClient) { }

  getInfo(): Observable<InterfaceI[]>{
    return this.http.get<InterfaceI[]>('./assets/data.json')
  }


}
