import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeEj1Component } from './tree-ej1.component';

describe('TreeEj1Component', () => {
  let component: TreeEj1Component;
  let fixture: ComponentFixture<TreeEj1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreeEj1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeEj1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
