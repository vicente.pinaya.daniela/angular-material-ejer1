import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatSelectModule} from '@angular/material/select';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
//import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSelectModule,
    MatSnackBarModule,
    MatInputModule,
    MatButtonModule,
    MatTreeModule,
    MatIconModule,
    //MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule
  ],

  exports:[

    MatSelectModule,
    MatSnackBarModule,
    MatInputModule,
    MatButtonModule,
    MatTreeModule,
    MatIconModule,
    //MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule

]

})
export class SharedModule { }
